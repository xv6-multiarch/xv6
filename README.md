# XV6-MultiArch

This is an attempt to merge the sources of multiple ports
of the XV6 operating system to different architectures
like i386, armv6, arm64, x86_64,...

The purpose is to offer teachers the ability to use which
architecture seems the most appropriate for theirs students.

A future goal is the execution of XV6 on real hardware, 
such as RPi-like boards, cheap tablets or simple smartphones.

The current version of xv6-multiarch compiles and
start on QEmu, on both `i386` and `arm` architectures.

The code for i386 architecture and user's programs come from 
original MIT XV6 project (https://github.com/mit-pdos/xv6-public)
and the code for `arm` architecture comes from Zhiyi Huang port
on Raspberry Pi platform (https://github.com/zhiyihuang/xv6_rpi_port).

Our work remains mainly on structure of directories and makefiles,
with few changes in header files (`fs.h` and `param.h`), and
corresponding clean-up.

You may test it with the make targets: `run`, or `debug` and `gdb`.

The compiling relies on Gnu GCC cross-compiling suite and
the execution on Qemu multiarch emulator.

## TODO

- [ ] add another port (perhaps xv6-mips, because its
	project's code is close to original xv6 project, 
	but we need first to make it compile and run on GCC/Qemu stuff)
- [ ] factorisation of kernel between all architectures, within the `kernel`
	directory, in order to limit the specific parts and offer to students
	a common set of files to modify the OS's behavior.
- [ ] check the teachingness of the project:
	- objdump of appropriate files (for debug)
	- needed files in xv6-book & xv6-rev (for each ARCH)
	- adequacy of the project's structure (place of headers, folders,...)
- [ ] add support for main real hardware devices (SDCard disks, USB keyboards, 
		VGA|HDMI video ports, ...). We may need a `devices` folder.
