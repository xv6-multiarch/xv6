# Kernel objects to be made in order to detect their presence
# and changes, for use by root Makefile
KOBJS = \
	string.o\
	arm.o\
	asm.o\
	bio.o\
	buddy.o\
	console.o\
	exec.o\
	file.o\
	fs.o\
	log.o\
	main.o\
	memide.o\
	pipe.o\
	proc.o\
	spinlock.o\
	start.o\
	swtch.o\
	syscall.o\
	sysfile.o\
	sysproc.o\
	trap_asm.o\
	trap.o\
	vm.o \
	picirq.o \
	timer.o \
	uart.o
