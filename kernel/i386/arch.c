/*
 * arch.c
 *
 * ASM routines which need to be visible
 * from generic programs (like console.c,..)
 *
 *  Created on: 26 avr. 2019
 *      Author: vmahe@free.fr
 */

#include "arch.h"

#include "memlayout.h"
#include "../types.h"

// Routines to let C programs use special x86 instructions.

//uchar
//in_b(ushort port)
//{
//  uchar data;
//
//  asm ("in %1,%0" : "=a" (data) : "d" (port));
//  return data;
//}
//
//void
//in_sl(int port, void *addr, int cnt)
//{
//  asm ("cld; rep insl" :
//               "=D" (addr), "=c" (cnt) :
//               "d" (port), "0" (addr), "1" (cnt) :
//               "memory", "cc");
//}
//
//void
//out_b(ushort port, uchar data)
//{
//  asm ("out %0,%1" : : "a" (data), "d" (port));
//}
//
//void
//out_sl(int port, const void *addr, int cnt)
//{
//  asm ("cld; rep outsl" :
//               "=S" (addr), "=c" (cnt) :
//               "d" (port), "0" (addr), "1" (cnt) :
//               "cc");
//}
//
//void
//s_to_sb(void *addr, int data, int cnt)
//{
//  asm ("cld; rep stosb" :
//               "=D" (addr), "=c" (cnt) :
//               "0" (addr), "1" (cnt), "a" (data) :
//               "memory", "cc");
//}
//
//void
//s_to_sl(void *addr, int data, int cnt)
//{
//  asm ("cld; rep stosl" :
//               "=D" (addr), "=c" (cnt) :
//               "0" (addr), "1" (cnt), "a" (data) :
//               "memory", "cc");
//}

void
setcli(void)
{
  asm ("cli");
}

void
setsti(void)
{
  asm ("sti");
}
