# Kernel objects to be made in order to detect their presence
# and changes, for use by root Makefile
# NB: their order is critical for LD to find references within them
KOBJS = \
	arch.o\
	exec.o\
	ide.o\
	kalloc.o\
	kbd.o\
	lapic.o\
	main.o\
	mp.o\
	picirq.o\
	proc.o\
	sleeplock.o\
	spinlock.o\
	string.o\
	swtch.o\
	syscall.o\
	trap.o\
	trapasm.o\
	uart.o\
	video.o\
	vm.o\
	bio.o\
	console.o\
	file.o\
	fs.o\
	ioapic.o\
	log.o\
	pipe.o\
	sysfile.o\
	sysproc.o\
	vectors.o\
