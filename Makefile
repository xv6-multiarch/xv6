######### XV6 MultiArch main Makefile ########
#
# It is intented to provide build and execution
# of XV6 on multiple architectures, including
# x86 and ARM, both 32 and 64 bits.
# It relies upon an "arch" subdirectory
# containing code files specific to the architecture
#

# You may pass the chosen architecture as an environment variable
# or force it there:
#export ARCH = x86_64
#export ARCH = arm
#export ARCH = aarch64
#export ARCH = mipsel

# default architecture remains i386, as targeted
# by MIT-PDOS for their original XV6
ifndef ARCH
	ARCH = i386
	export ARCH
endif

# load the lists of objects to be done
include kernel/$(ARCH)/kobjslist.mk
include usr/uprogslist.mk

#### Hierarchy of objects to be built ####
#
# The objects relationships can be summarize as this:
#  - User programs need system calls and main library (ulib)
#  - System calls and main library rely on kernel objects
#  - File system needs user programs to embedd them
#  - The kernel is facing two cases:
#     1. the file system is in memory => fs.img is linked in the kernel
#   and must be done before the kernel linking. This is the default case,
#   as other ways need more stuff to work.
#     2. the file system is on (possibly emulated) hardware disk => kernel
#   can be linked independently from fs.img. This was the original XV6 choice.
#   But it implies coding the access to hardware disk (like IDE), which relies
#   on the real architecture target.
#
# So the generic hierarchy can be schemed as:
#   kobjs -> ulibs -> uprogs -> fs.img -> kernel -> emulation
#
# Targetting real hardware (as MIT XV6) may imply additional builds, with
# different hierarchies. They may be added later, at the end of this file.
#
# In order for the user programs and kernel linker to find the needed files,
# all binaries are generated within dedicated folders, independent from ARCH:
#  - kernel/bin : for kernel binaries, including usys object
#  - usr/bin : for user space binaries, including ulib object
# As each architecture may have specific source files and objects, the
# Makefile for kernel objects is within its ARCH subfolder of kernel/,
# together with the kernel.ld script (which is called from the root Makefile).
#
# All those considerations explain the structure of this root Makefile
# and the generous use of pipes (|) within it.
#
# NB : the piping make utility may not properly handle some changes of files
# so, in case of troubles, try to enforce the work by cleaning the whole project.

# Binaries folders to be used by sub Makefiles so compute absolute path
ROOT_DIR:=$(CURDIR)
export KBIN = $(ROOT_DIR)/kernel/bin
export UBIN = $(ROOT_DIR)/usr/bin

### Tools dedicated to the chosen architecture ###
# We rely on GNU GCC toolchain as packaged for cross-compiling
# both for testing purpose and to use architecure
# name within those tools (ie "aarch64" for ARM 64 bits).
#
# We target cross-compiling as default in order to
# be able to build XV6 for all targeted architectures
# on all targeted architectures (ie build/run i386 XV6 on ARM)

# parameters for i386 architecture
ifeq ($(ARCH),i386)
	# On recent Debian based distributions, you may have to create the link 
	#	sudo ln -s /usr/bin/i686-linux-gnu-gcc-9 /usr/bin/i686-linux-gnu-gcc
	TOOLPREFIX = i686-linux-gnu-
	# force compiling to 32 bits despite a 64 bits machine
	# ARM kernel does run in loop if -fno-omit-frame-pointer
	CFLAGS = -m32 -fno-omit-frame-pointer
	ASFLAGS = -m32 -gdwarf-2 -Wa,-divide
	# FreeBSD ld wants ``elf_i386_fbsd''
	LDFLAGS = -m $(shell $(LD) -V | grep elf_i386 2>/dev/null | head -n 1)
	GRAPHICS = yes
	BOOT_MODE = img
endif

# parameters for ARMv6 architecture
ifeq ($(ARCH),arm)
	# test of generic GCC cross-compiling tools on Linux distributions
	# Rem: it may have an impact for libraries (using glibc vs dedicated one)
	TOOLPREFIX = arm-none-eabi-
#	TOOLPREFIX = arm-linux-gnueabi-
	# force compiling to 32 bits despite a 64 bits machine
	CFLAGS = -march=armv6
	ASFLAGS = -march=armv6
	LDFLAGS = -L.
	GRAPHICS = no
	BOOT_MODE = direct
	# suppress pulse adio error messages
	export QEMU_AUDIO_DRV=none
endif

ifndef CPUS
CPUS := 1
endif
export CPUS

CC = $(TOOLPREFIX)gcc
AS = $(TOOLPREFIX)gas
LD = $(TOOLPREFIX)ld
OBJCOPY = $(TOOLPREFIX)objcopy
OBJDUMP = $(TOOLPREFIX)objdump

CFLAGS += -fno-pic -static -fno-builtin -fno-strict-aliasing -O2 -Wall -MD -ggdb -I.
CFLAGS += $(shell $(CC) -fno-stack-protector -E -x c /dev/null >/dev/null 2>&1 && echo -fno-stack-protector)
ASFLAGS += -I.
LDFLAGS += -L.

# host compiler
HOSTCC_preferred = gcc
define get_hostcc
    $(if $(shell which $(HOSTCC_preferred)),$(HOSTCC_preferred),"cc")
endef
HOSTCC := $(call get_hostcc)

# Disable PIE when possible (for Ubuntu 16.10 toolchain)
ifneq ($(shell $(CC) -dumpspecs 2>/dev/null | grep -e '[^f]no-pie'),)
CFLAGS += -fno-pie -no-pie
endif
ifneq ($(shell $(CC) -dumpspecs 2>/dev/null | grep -e '[^f]nopie'),)
CFLAGS += -fno-pie -nopie
endif

# needed by sub-directories make processes
export CC CFLAGS AS ASFLAGS LD LDFLAGS OBJCOPY OBJDUMP
export QEMU QEMUFLAGS


# making of user programs (including usys.o)
.PHONY: progs
progs:
	$(shell mkdir -p $(UBIN))
	$(MAKE) -C usr progs

## making of kernel objects (including usys.o)
#$(UBIN)/usys.o:
#	$(shell mkdir -p $(UBIN))
#	$(MAKE) -C usr $(UBIN)/usys.o
#
## making of user objects and linking -> final progs
#_%: $(UBIN)/usys.o
#	$(shell mkdir -p $(UBIN))
#	$(MAKE) -C usr $@
#
## making of ulib.o and linking     [done by usr .o rule]
#$(UBIN)/ulib.o: $(KBIN)/usys.o
#	$(shell mkdir -p $(UBIN))
#	$(MAKE) -C usr $(UBIN)/ulib.o

$(KBIN):
	mkdir $(KBIN)

# assembling the file system by collecting all _* files in usr/bin
# This way gives the possibility to add any other test program to the "disk"
fs.img: $(KBIN)/mkfs README progs
	# packing user progs within usr/bin directory
	cp README $(UBIN)/README
	cd $(UBIN) && pwd && $(KBIN)/mkfs $(UBIN)/fs.img README $(shell ls $(UBIN)/ | grep _)
	# we are in the XV6 root directory
	cp $(UBIN)/fs.img fs.img

# the utility to make fs.img is executed on coder's computer
$(KBIN)/mkfs: kernel/$(ARCH)/mkfs.c kernel/$(ARCH)/fs.h
	$(shell mkdir -p $(KBIN))
	$(HOSTCC) -Werror -Wall -o $(KBIN)/mkfs kernel/$(ARCH)/mkfs.c

# making of the default kernel with embedded file system (ARCH specific)
#xv6memfs.img: $(KBIN) fs.img
#	$(MAKE) -C kernel/$(ARCH) xv6memfs.img
#
#xv6.img: $(KBIN) fs.img
#	$(MAKE) -C kernel/$(ARCH) xv6.img
#
#xv6kernel: $(KBIN)
#	$(MAKE) -C kernel/$(ARCH) $(KBIN)/xv6kernel
#	cp $(KBIN)/kernel xv6kernel
#
#kernelmemfs: $(KBIN) fs.img
#	$(MAKE) -C kernel/$(ARCH) $(KBIN)/kernelmemfs
#	cp $(KBIN)/kernelmemfs kernelmemfs

.PHONY: clean
clean:
	rm -f fs.img xv6memfs.img xv6.img xv6kernel kernelmemfs
	rm -rf $(KBIN)
	make -C kernel/$(ARCH) clean
	rm -rf $(UBIN)
	make -C usr clean

## booting sequence depends on architecture target => defined in ARCH
#ifeq ($(ARCH),arm)
#qemu-memfs: kernelmemfs
#	$(QEMU) $(QEMUFLAGS) -gdb tcp::26000
#else
#qemu-memfs: xv6memfs.img
#	$(QEMU) $(QEMUFLAGS) -gdb tcp::26000
#endif
######## debugging XV6 with GDB #######
## try to generate a unique GDB port
#GDBPORT = $(shell expr `id -u` % 5000 + 25000)
## QEMU's gdb stub command line changed in 0.11
#QEMUGDB = $(shell if $(QEMU) -help | grep -q '^-gdb'; \
#	then echo "-gdb tcp::$(GDBPORT)"; \
#	else echo "-s -p $(GDBPORT)"; fi)
#
#QEMUOPTS = -drive file=fs.img,index=1,media=disk,format=raw -smp $(CPUS) -m 512 $(QEMUEXTRA)
#QEMUIMG = -drive file=xv6.img,index=0,media=disk,format=raw
#
#.gdbinit: .gdbinit.tmpl
#	sed "s/localhost:1234/localhost:$(GDBPORT)/" < $^ > $@
#
#qemu-gdb: fs.img xv6.img .gdbinit
#	@echo "*** Now run 'gdb'." 1>&2
#	$(QEMU) -serial mon:stdio $(QEMUOPTS) -S $(QEMUGDB) $(QEMUIMG)
#
#qemu-nox-gdb: fs.img xv6.img .gdbinit
#	@echo "*** Now run 'gdb'." 1>&2
#	$(QEMU) -nographic $(QEMUOPTS) -S $(QEMUGDB) $(QEMUIMG)

run: fs.img
	$(MAKE) -C kernel/$(ARCH) run

debug: fs.img
	$(MAKE) -C kernel/$(ARCH) debug

# command to run the GDB appropriated to ARCH
gdb:
	$(MAKE) -C kernel/$(ARCH) gdb
